/**
 * Reverses an array
 * @param {*} arr The array to reverse
 * @returns A new, reversed array
 */
const reverseArray = (arr) => {
  if (!arr || arr.length === 0) {
    return null;
  }

  const retArr = new Array(arr.length);

  for (let i = 0; i < arr.length / 2; i++) {
    const temp = arr[i];
    retArr[i] = arr[arr.length - i - 1];
    retArr[arr.length - i - 1] = temp;
  }

  return retArr;
};

console.log(reverseArray([])); // null
console.log(reverseArray(['Apple', 'Banana', 'Orange', 'Coconut'])); //[ 'Coconut', 'Orange', 'Banana', 'Apple' ]
console.log(reverseArray(['Apple', 'Banana', 'Orange', 'Coconut', 'Peach'])); //[ 'Peach', 'Coconut', 'Orange', 'Banana', 'Apple' ]

/**
 * Checks if a string is a palindrom
 * @param {*} str The string to check
 * @returns True whether the string is a palindrome, false if not
 */
const isPalindrome = (str) => {
  if (!str) {
    return false;
  }

  str = str.replaceAll(" ", "").toLowerCase();

  for (let i = 0; i < str.length / 2; i++) {
    if (str[i] !== str[str.length - i - 1]) {
      return false;
    }
  }

  return true;
};

console.log(isPalindrome("")); // false
console.log(isPalindrome("Level")); // true
console.log(isPalindrome("Levels")); // false
console.log(isPalindrome("Yo banana boy")); // true
