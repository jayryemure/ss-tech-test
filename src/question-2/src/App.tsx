import * as React from 'react';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import { CatFacts } from "./CatFacts";

export default function App() {
  return (
    <Container maxWidth="lg">
      <Box sx={{ my: 4 }}>
        <CatFacts />
      </Box>
    </Container>
  );
}