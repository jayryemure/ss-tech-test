import React, { useState, useEffect } from "react";
import { styled } from "@mui/material/styles";
import CircularProgress from "@mui/material/CircularProgress";
import Alert from "@mui/material/Alert";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export const CatFacts = () => {
  const [catFacts, setCatFacts] = useState<any[]>([]);
  const [loading, setIsLoading] = useState<boolean>();

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      // Timer to simulate latency
      await new Promise((r) => setTimeout(r, 2000));

      const result = await fetch("https://catfact.ninja/facts?limit=9");
      setIsLoading(false);
      const { data } = await result.json();

      // Sort cat data by fact
      data.sort((a, b) => {
        return a.fact.localeCompare(b.fact);
      });

      setCatFacts(data);
    };

    fetchData();
  }, []);

  return (
    <div>
      {!loading && !catFacts && (
        <Alert severity="error">Something went wrong!</Alert>
      )}
      {loading && (
        <Box sx={{ display: "flex" }}>
          <CircularProgress />
        </Box>
      )}
      {catFacts && (
        <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, md: 12 }}>
          {catFacts.map((fact, index) => {
            return (
              <Grid item xs={4} md={4} key={index}>
                <Item>
                  <Box sx={{ minWidth: 275 }}>
                    <Card variant="outlined">
                      <CardContent>
                        <Typography
                          sx={{ fontSize: 14 }}
                          color="text.secondary"
                          gutterBottom
                        >
                          {fact.fact}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Box>
                </Item>
              </Grid>
            );
          })}
        </Grid>
      )}
    </div>
  );
};
